provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

resource "aws_instance" "ec2" {
  ami                         = "${var.image_id}"
  instance_type               = "t2.micro"
  key_name                    = "${var.key_name}"
  security_groups             = [ "al-office" ]
  associate_public_ip_address = true
  tags {
    Name = "jojo"
  }
  user_data = <<HEREDOC
  #!/bin/bash
  yum -y install httpd
  service httpd start
  systemctl start httpd
  systemctl enable httpd
HEREDOC
}
