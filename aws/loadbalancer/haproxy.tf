data "template_file" "haproxy" {
  template = "${file("./templates/haproxy.cfg")}"

  vars {
    apacheip = "${aws_instance.apache.public_ip}"
    nginxip  = "${aws_instance.nginx.public_ip}"
  }
}

resource "aws_instance" "haproxy" {
  depends_on                  = ["data.template_file.haproxy"]
  ami                         = "${lookup(var.amis, var.region)}"
  instance_type               = "t2.micro"
  key_name                    = "${var.key_name}"
  security_groups             = [
    "${list(
      "${aws_security_group.worldssh.id}",
      "${aws_security_group.publichttp.id}"
      )}"]
  associate_public_ip_address = true
  subnet_id                   = "${var.publicsubnet}"
  tags {
    Name = "jthaproxy"
  }
  user_data = "${base64encode(data.template_file.haproxy.rendered)}"
}


output "haproxy_address" {
  value = "${aws_instance.haproxy.public_ip}"
}

# output "template_file_output" {
#   value = "${data.template_file.haproxy.rendered}"
# }
