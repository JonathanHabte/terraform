resource "aws_instance" "nginx" {
  ami                         = "${lookup(var.amis, var.region)}"
  instance_type               = "t2.micro"
  key_name                    = "${var.key_name}"
  security_groups             = [
    "${list(
      "${aws_security_group.worldssh.id}",
      "${aws_security_group.publichttp.id}"
      )}"]
  subnet_id                   = "${var.publicsubnet}"
  associate_public_ip_address = true
  tags {
    Name = "jtnginx"
  }
  user_data = <<HEREDOC
  #!/bin/bash
  sudo amazon-linux-extras install -y nginx1.12
  sudo service nginx start
HEREDOC
}
