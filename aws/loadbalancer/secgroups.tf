resource "aws_security_group" "worldssh" {
  name        = "worldssh"
  description = "Allow ssh from anywhere"
  vpc_id      = "${var.vpc}"
  # "${aws_vpc.main.id}"

  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name    = "jtworldssh"
  }
}

resource "aws_security_group" "publichttp" {
  name        = "publichttp"
  description = "Allow HTTP from anywhere"
  vpc_id      = "${var.vpc}"
  # "${aws_vpc.main.id}"

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags {
    Name    = "jtpublicHTTP"
  }
}
