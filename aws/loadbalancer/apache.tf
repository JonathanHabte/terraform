resource "aws_instance" "apache" {
  # depends_on                  = [
  #   "${aws_security_group.worldssh}",
  #   "${aws_security_group.publicHTTPgroup}"
  #   ]
  ami                         = "${lookup(var.amis, var.region)}"
  instance_type               = "t2.micro"
  key_name                    = "${var.key_name}"
  security_groups             = [
    "${list(
      "${aws_security_group.worldssh.id}",
      "${aws_security_group.publichttp.id}"
      )}"]
  associate_public_ip_address = true
  subnet_id                   = "${var.publicsubnet}"
  tags {
    Name = "jtapache"
  }
  user_data = <<HEREDOC
  #!/bin/bash
  sudo yum -y install httpd
  sudo service httpd start
  sudo systemctl start httpd
  sudo systemctl enable httpd
HEREDOC
}
