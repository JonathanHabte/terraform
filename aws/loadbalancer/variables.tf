variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "us-east-1"
}


provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

variable "key_name" {
  default = "TommyNV"
}

variable "vpc" {
  default = "vpc-eb563690"
}

variable "publicsubnet" {
  default = "subnet-01bee13857f2290de"
}

variable "amis" {
  type = "map"
  default = {
    "eu-west-2" = "ami-0274e11dced17bb5b"
    "us-east-1" = "ami-009d6802948d06e52"
  }
}
