variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "us-east-1"
}

variable "AZ" {
  default = "us-east-1a"
}

variable "key_name" {
  default = "TommyNV"
}

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

variable "amis" {
  type = "map"
  default = {
    "eu-west-2" = "ami-0274e11dced17bb5b"
    "us-east-1" = "ami-009d6802948d06e52"
  }
}

variable "vpccidr" {
  description = "Make the subnets with these cidrs"
  default = "10.1.0.0/16"
}

variable "subnetcidr" {
  description = "Make the subnets with these cidrs"
  default = "10.1.0.0/17"
}
