resource "aws_vpc" "main" {
  cidr_block       = "${var.vpccidr}"

  tags {
    Name = "jontomvpc2"
  }
}
