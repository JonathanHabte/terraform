resource "aws_launch_configuration" "launchconfig" {
  image_id                    = "${lookup(var.amis, var.region)}"
  instance_type               = "t2.micro"
  key_name                    = "${var.key_name}"
  security_groups             =
      ["${list(
      "${aws_security_group.worldssh.id}",
      "${aws_security_group.publichttp.id}"
      )}"]
  associate_public_ip_address = true

  user_data = <<HEREDOC
  #!/bin/bash
  sudo yum -y install httpd
  sudo service httpd start
  sudo systemctl start httpd
  sudo systemctl enable httpd
HEREDOC
}

resource "aws_autoscaling_group" "asg" {
  name                 = "jt_aws_autoscaling_group"
  launch_configuration = "${aws_launch_configuration.launchconfig.name}"
  load_balancers       = ["${aws_elb.elb.name}"]
  min_size             = 1
  max_size             = 5
  vpc_zone_identifier       = ["${aws_subnet.jontomsubnet.id}"]
  tags = [
    {
      key                 = "Name"
      value               = "jt-autoscaling-instance"
      propagate_at_launch = true
    },
    {
      key                 = "Name"
      value               = "jt-autoscaling-group"
      propagate_at_launch = false
    },
  ]
}
