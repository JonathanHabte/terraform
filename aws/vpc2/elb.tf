resource "aws_elb" "elb" {
  name               = "jt-elb"
  subnets            = ["${aws_subnet.jontomsubnet.id}"]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 5
    timeout             = 5
    target              = "HTTP:80/"
    interval            = 30
  }
  # cross_zone_load_balancing   = true

  security_groups      =
      ["${aws_security_group.publichttp.id}"]

  tags {
    Name = "foobar-terraform-elb"
  }
}
