resource "aws_route_table" "routetable" {
  vpc_id = "${aws_vpc.main.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }
  # route {
  #   cidr_block = "0.0.0.0/0"
  #   gateway_id = "${aws_nat_gateway.ngw.id}"
  # }
  tags {
    Name = "jontomroute2"
  }
}

resource "aws_route_table_association" "attachsubnets" {
  subnet_id      = "${aws_subnet.jontomsubnet.id}"
  route_table_id = "${aws_route_table.routetable.id}"
}
