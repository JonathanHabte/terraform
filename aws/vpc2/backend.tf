terraform {
  backend "s3" {
    bucket = "jontombucket"
    key    = "vpc2/terraform.tfstate"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "ec2" {
  backend = "s3"
  config {
    bucket = "jontombucket"
    key    = "vpc2/terraform.tfstate"
    region = "us-east-1"
  }
}
