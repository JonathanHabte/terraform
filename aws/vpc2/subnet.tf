resource "aws_subnet" "jontomsubnet" {
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "${var.subnetcidr}"

  tags {
    Name = "jontonsubnet2"
  }
}
