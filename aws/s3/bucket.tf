provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}
resource "aws_s3_bucket" "bucket" {
  bucket = "jontomtest"
  acl    = "private"

  tags {
    Name        = "jontombucket"
    Environment = "Dev"
  }
}
