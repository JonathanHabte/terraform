terraform {
  backend "s3" {
    bucket = "jontombucket"
    key    = "s3/terraform.tfstate"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "bucket" {
  backend = "s3"
  config {
    bucket = "jontombucket"
    key    = "s3/terraform.tfstate"
    region = "us-east-1"
  }
}
