resource "aws_eip" "eip" {
  vpc      = true
}

resource "aws_nat_gateway" "ngw" {
  allocation_id = "${aws_eip.eip.id}"
  subnet_id     = "${aws_subnet.jontomsubnets.1.id}"
  depends_on    = ["aws_internet_gateway.igw"]
  depends_on    = ["aws_subnet"]

}
