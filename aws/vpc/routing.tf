resource "aws_route_table" "routetable" {
  count = 2
  vpc_id = "${aws_vpc.main.id}"

  tags {
    Name = "jontomroute"
  }
}
resource "aws_route" "route1" {
  route_table_id       = "${aws_route_table.routetable.0.id}"
  gateway_id = "${aws_internet_gateway.igw.id}"
  destination_cidr_block = "0.0.0.0/0"

}
resource "aws_route" "route2" {
  route_table_id       = "${aws_route_table.routetable.1.id}"
  nat_gateway_id = "${aws_nat_gateway.ngw.id}"
  destination_cidr_block = "0.0.0.0/0"

}

resource "aws_route_table_association" "attachsubnets" {
  count = 2
  subnet_id      = "${element(aws_subnet.jontomsubnets.*.id, count.index)}"
  route_table_id = "${element(aws_route_table.routetable.*.id, count.index)}"
}
