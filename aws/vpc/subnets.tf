resource "aws_subnet" "jontomsubnets" {
  count = 2
  vpc_id     = "${aws_vpc.main.id}"
  cidr_block = "${element(var.cidrs, count.index)}"

  tags {
    Name = "${element(var.subnettags, count.index)}"
  }
}
