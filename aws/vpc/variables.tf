variable "access_key" {}
variable "secret_key" {}
variable "region" {
  default = "us-east-1"
}

provider "aws" {
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
  region     = "${var.region}"
}

variable "cidrs" {
  description = "Make the subnets with these cidrs"
  type = "list"
  default = ["10.0.0.0/28", "10.0.0.16/28"]
}

variable "subnettags" {
  description = "Make the subnets with these tags"
  type = "list"
  default = ["jontompublic", "jontomprivate"]
}

variable "cidrs" {
  description = "Make the subnets with these cidrs"
  type = "list"
  default = ["10.0.0.0/28", "10.0.0.16/28"]
}
