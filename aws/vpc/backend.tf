terraform {
  backend "s3" {
    bucket = "jontombucket"
    key    = "vpc/terraform.tfstate"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "main" {
  backend = "s3"
  config {
    bucket = "jontombucket"
    key    = "vpc/terraform.tfstate"
    region = "us-east-1"
  }
}
