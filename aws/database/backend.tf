terraform {
  backend "s3" {
    bucket = "jontombucket"
    key    = "database/terraform.tfstate"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "default" {
  backend = "s3"
  config {
    bucket = "jontombucket"
    key    = "database/terraform.tfstate"
    region = "us-east-1"
  }
}
