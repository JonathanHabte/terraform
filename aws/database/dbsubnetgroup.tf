resource "aws_db_subnet_group" "jontomDBgroup" {

  name       = "jontonsubnet"
  subnet_ids = ["subnet-b6bfa5ce", "subnet-8a08f3e3"]

  tags {
    Name = "jontom DB subnet group"
  }
}
