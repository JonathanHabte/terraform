resource "aws_db_instance" "default" {
  depends_on           = ["aws_db_subnet_group.jontomDBgroup"]
  allocated_storage    = 5
  engine               = "mariadb"
  engine_version       = "10.3"
  instance_class       = "db.t2.small"
  name                 = "mydb"
  username             = "jontom"
  password             = "secretsecret"
  skip_final_snapshot  = "true"
  publicly_accessible  = "true"
  vpc_security_group_ids = [ "sg-7d743215" ]
  db_subnet_group_name = "${aws_db_subnet_group.jontomDBgroup.id}"
  tags {
    Name = "jontomDB"
  }
}
