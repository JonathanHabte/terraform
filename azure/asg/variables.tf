variable "prefix" {
  default = "tomcatasg"
}

variable "location" {
  default = "UKwest"
}

variable "vmsize" {
  default = "Standard_B1s"
}
