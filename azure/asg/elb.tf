resource "azurerm_lb" "main" {
  name                = "${var.prefix}-LoadBalancer"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  frontend_ip_configuration {
    name                 = "${var.prefix}-lbPublicIPAddress"
    # public_ip_address_id = "${azurerm_public_ip.main.id}"
    subnet_id = "${azurerm_subnet.internal.id}"
  }
}
