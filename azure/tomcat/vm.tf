# Create virtual machine

resource "azurerm_virtual_machine" "main" {
  name                  = "${var.prefix}-vm"
  location              = "${azurerm_resource_group.main.location}"
  resource_group_name   = "${azurerm_resource_group.main.name}"
  network_interface_ids = ["${azurerm_network_interface.main.id}"]
  vm_size               = "${var.vmsize}"
  availability_set_id   = "${azurerm_availability_set.main.id}"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true


  # Uncomment this line to delete the data disks automatically when deleting the VM
  delete_data_disks_on_termination = true


  storage_image_reference {
    publisher = "RedHat"
    offer     = "RHEL"
    sku       = "7.4"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${var.prefix}-os-disk"
    # caching           = "ReadWrite"
    create_option     = "FromImage"
    # managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "${var.prefix}-computer"
    admin_username = "${var.prefix}"
    admin_password = "Tomtommyfax1!"
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }


  tags {
    environment = "prod"
    name = "${var.prefix}-instance"
  }
}

# install apache

resource "azurerm_virtual_machine_extension" "cs_apache" {
  name 			= "${var.prefix}-apache"
  location 		= "${var.location}"
  resource_group_name   = "${azurerm_resource_group.main.name}"
  virtual_machine_name  = "${azurerm_virtual_machine.main.name}"
  publisher 		= "Microsoft.OSTCExtensions"
  type 			= "CustomScriptForLinux"
  type_handler_version  = "1.2"

  settings = <<SETTINGS
  {
   "commandToExecute": "sh start_groovy_apache.sh",
   "fileUris": ["https://bitbucket.org/JonathanHabte/terraform/raw/e9212f39b35b9dc9e2b98647199cb7f089f08d35/azure/start_groovy_apache.sh"
   ]
  }
SETTINGS

  tags {
	group = "ALacademy"
  }
}
