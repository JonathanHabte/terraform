resource "azurerm_resource_group" "main" {
  name     = "${var.prefix}-resources"
  location = "${var.location}"
}

resource "azurerm_virtual_network" "main" {
  name                = "${var.prefix}-network"
  address_space       = ["10.0.0.0/16"]
  location            = "${azurerm_resource_group.main.location}"
  resource_group_name = "${azurerm_resource_group.main.name}"
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  resource_group_name  = "${azurerm_resource_group.main.name}"
  virtual_network_name = "${azurerm_virtual_network.main.name}"
  address_prefix       = "10.0.2.0/24"
}

resource "azurerm_network_interface" "main" {
  name                = "${var.prefix}-nic"
  location            = "${azurerm_resource_group.main.location}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  ip_configuration {
    name                          = "${var.prefix}-ipconfig"
    subnet_id                     = "${azurerm_subnet.internal.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.main.id}"
    load_balancer_backend_address_pools_ids = ["${azurerm_lb_backend_address_pool.backend_pool.id}"]

  }
}

resource "azurerm_network_interface" "main1" {
  name                = "${var.prefix}-nic1"
  location            = "${azurerm_resource_group.main.location}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  ip_configuration {
    name                          = "${var.prefix}-ipconfig1"
    subnet_id                     = "${azurerm_subnet.internal.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = "${azurerm_public_ip.main1.id}"
    load_balancer_backend_address_pools_ids = ["${azurerm_lb_backend_address_pool.backend_pool.id}"]

  }
}



resource "azurerm_public_ip" "main" {
    name                         = "${var.prefix}-PublicIP"
    location                     = "${var.location}"
    resource_group_name          = "${azurerm_resource_group.main.name}"
    public_ip_address_allocation = "dynamic"

    tags {
        environment = "prod"
        name = "${var.prefix}-publicip"
    }
}

resource "azurerm_public_ip" "main1" {
    name                         = "${var.prefix}-PublicIP1"
    location                     = "${var.location}"
    resource_group_name          = "${azurerm_resource_group.main.name}"
    public_ip_address_allocation = "dynamic"

    tags {
        environment = "prod"
        name = "${var.prefix}-publicip1"
    }
}


resource "azurerm_availability_set" "main" {
  name                = "${var.prefix}-availabilityset"
  location            = "${azurerm_resource_group.main.location}"
  resource_group_name = "${azurerm_resource_group.main.name}"
  managed = "true"
  platform_fault_domain_count = 2

  tags {
    environment = "Production"
  }
}
