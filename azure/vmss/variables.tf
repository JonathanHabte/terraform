variable "prefix" {
  default = "tomcat"
}

variable "location" {
  default = "UKwest"
}

variable "vmsize" {
  default = "Standard_B1s"
}
