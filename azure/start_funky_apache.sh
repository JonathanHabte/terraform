#!/bin/bash

# disable firewall
echo "Installing firewalld"
yum install firewalld

echo "Disabling firewall"
firewall-cmd --zone=public --add-service=http
firewall-cmd --zone=public --permanent --add-service=http


echo "Curl azureclient rpm"
curl -o azureclient.rpm https://rhui-1.microsoft.com/pulp/repos/microsoft-azure-rhel7/rhui-azure-rhel7-2.2-74.noarch.rpm
rpm -U azureclient.rpm

echo "Installing apache"
yum -y install httpd

cat >/var/www/html/index.html <<TOMCAT
<html>
<h1>Tommy's first webpage</h1>
<p>This tomcat's funky page</p>
</html>
TOMCAT

service httpd start
systemctl start httpd
systemctl enable httpd
